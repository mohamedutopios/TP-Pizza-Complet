﻿using BlazorPizzas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BlazorPizzas.Services
{
    public class HttpPizzaManager : IPizzaManager
    {
        private readonly HttpClient client;

        public HttpPizzaManager(
            HttpClient client)
        {
            this.client = client;
        }

        public async Task<bool> AddOrUpdate(Pizza p)
        {
            var response = await client.PostAsJsonAsync("api/pizzas", p);

            return response.IsSuccessStatusCode;
        }

        public Task<IEnumerable<Pizza>> GetPizzas()
        {
            return client.GetFromJsonAsync<IEnumerable<Pizza>>("api/pizzas");
        }
    }
}
