using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BlazorPizzas.Data;
using BlazorPizzas.Services;
using BlazorPizzas.Opts;
using Microsoft.Extensions.Options;
using System.Net.Http;
using Polly.Extensions.Http;
using Polly;

namespace BlazorPizzas
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();

            services.AddOptions();
            services.Configure<ApiOptions>(Configuration.GetSection("Api"));

            services.AddSingleton<IPizzaManager, InMemoryPizzaManager>();
            //  services.AddHttpClient<IPizzaManager, HttpPizzaManager>((sp, client) =>
            //{
            //  var options = sp.GetRequiredService<IOptions<ApiOptions>>();

            //client.BaseAddress = new Uri(options.Value.Url);
            // })
            //   .AddPolicyHandler(GetPolicy());
            // }

            //    private IAsyncPolicy<HttpResponseMessage> GetPolicy()
            //     => HttpPolicyExtensions
            //     .HandleTransientHttpError()
            //      .WaitAndRetryAsync(3, i => TimeSpan.FromMilliseconds(300 + (i * 100)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
